﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Advent2021
{
    public static class Program
    {
        public static void Main()
        {
            var file = File.ReadAllLines(@"C:\Users\marku\Code\C#\Advent2021\input\day_04.txt");

            var numbers =
                file
                    .First()
                    .Split(',', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
                    .Select(int.Parse)
                    .ToList();

            var gridNumbers =
                file
                    .Skip(2)
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Select(x =>
                        x.Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries))
                    .Select(x => x.Select(int.Parse).ToList())
                    .ToList();

            var boards = new List<Board>();

            for (var y = 0; y < gridNumbers.Count - 5; y += 5)
            {
                var block = new List<List<GridNumber>>();

                for (var z = y; z < y + 5; z += 1)
                {
                    var row = new List<GridNumber>();
                    for (var i = 0; i < 5; i += 1)
                    {
                        row.Add(new GridNumber(gridNumbers[z][i], false));
                    }

                    block.Add(row);
                }

                boards.Add(new Board(block, false));
            }

            foreach (var item in numbers)
            {
                // Alters the boards list.
                CallNumber(item, boards);

                var (isWinner, boardNumber) = CheckWinner(boards);

                if (isWinner)
                {
                    Console.WriteLine(SumUnmarked(boards[boardNumber]) * item);
                    break;
                }
            }
        }

        // Checks every board to see if there is a winner and returns true and that board, else false.
        private static (bool IsWinner, int BoardNumber) CheckWinner(List<Board> boards)
        {
            var index = 0;
            foreach (var board in boards)
            {
                foreach (var grid in board.Grid)
                {
                    if (grid.All(x => x.Called))
                    {
                        return (true, index);
                    }
                }

                index += 1;
            }

            return (false, -1);
        }

        private static void CallNumber(int input, List<Board> boards)
        {
            foreach (var board in boards)
            {
                foreach (var grid in board.Grid)
                {
                    foreach (var number in grid)
                    {
                        if (number.Value == input)
                        {
                            number.Called = true;
                        }
                    }
                }
            }
        }

        private static int SumUnmarked(Board input) =>
            input.Grid
                .Select(x => x.Where(y => y.Called == false))
                .Select(x => x.Aggregate(0, (z, y) => y.Value + z))
                .Sum();
    }

    public class GridNumber
    {
        public int Value;
        public bool Called;

        public GridNumber(int value, bool called)
        {
            Value = value;
            Called = called;
        }
    }

    public class Board
    {
        public List<List<GridNumber>> Grid;
        public bool Winner;

        public Board(List<List<GridNumber>> grid, bool winner)
        {
            Grid = grid;
            Winner = winner;
        }
    }
}