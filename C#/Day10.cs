﻿namespace Advent2021;

public static class Day10
{
    private const string TestSyntax = @"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

    private const string SyntaxPath = @"C:\Users\marku\Code\C#\Advent2021\input\day_10.txt";

    // Scores
    private const int CURVE = 3;
    private const int SQUARE = 57;
    private const int CURLY = 1197;
    private const int ARROW = 25137;

    private enum BracketType
    {
        CURVE,
        SQUARE,
        CURLY,
        ARROW
    }

    public static void ModuleMain()
    {
        var solution = File.ReadAllLines(SyntaxPath).IncorrectSyntaxCounter().SyntaxScoreSum();
        //TestSyntax.Split(Environment.NewLine).IncorrectSyntaxCounter().SyntaxScoreSum();
        /*Debug.Assert(solution == 26397,
            $"Incorrect solution: {solution} does not equal 26397");*/
        Console.WriteLine(solution);
    }

    private static int[] IncorrectSyntaxCounter(this IReadOnlyList<string> syntaxToCheck)
    {
        var syntaxCounter = new int[4];
        // When you find a match pop if off of the stack.
        var syntaxStack = new Stack<char>();

        for (var x = 0; x < syntaxToCheck.Count; x += 1)
        {
            for (var y = 0; y < syntaxToCheck[x].Length; y += 1)
            {
                if (syntaxToCheck[x][y] is '(' or '[' or '{' or '<')
                {
                    syntaxStack.Push(syntaxToCheck[x][y]);
                }

                if (syntaxToCheck[x][y] is ')' or ']' or '}' or '>')
                {
                    if (syntaxStack.Peek() == '(' && syntaxToCheck[x][y] == ')'
                        || syntaxStack.Peek() == '[' && syntaxToCheck[x][y] == ']'
                        || syntaxStack.Peek() == '{' && syntaxToCheck[x][y] == '}'
                        || syntaxStack.Peek() == '<' && syntaxToCheck[x][y] == '>')
                    {
                        syntaxStack.Pop();
                    }
                    else
                    {
                        switch (syntaxToCheck[x][y])
                        {
                            case ')':
                                syntaxCounter[(int) BracketType.CURVE] += 1;
                                break;
                            case ']':
                                syntaxCounter[(int) BracketType.SQUARE] += 1;
                                break;
                            case '}':
                                syntaxCounter[(int) BracketType.CURLY] += 1;
                                break;
                            case '>':
                                syntaxCounter[(int) BracketType.ARROW] += 1;
                                break;
                        }

                        break;
                    }
                }
            }
        }

        return syntaxCounter;
    }

    private static int SyntaxScoreSum(this IReadOnlyList<int> checkedSyntax)
    {
        var curveScore = checkedSyntax[(int) BracketType.CURVE] * CURVE;
        var squareScore = checkedSyntax[(int) BracketType.SQUARE] * SQUARE;
        var curlyScore = checkedSyntax[(int) BracketType.CURLY] * CURLY;
        var arrowScore = checkedSyntax[(int) BracketType.ARROW] * ARROW;

        return curveScore + squareScore + curlyScore + arrowScore;
    }
}