﻿namespace Advent2021;

public static class Day09
{
    private const string TestData =
        @"2199943210
3987894921
9856789892
8767896789
9899965678";

    private enum Direction
    {
        Up = 0,
        Down = 1,
        Left = 2,
        Right = 3,
    }

    // Directions we will check.
    private static int Up(this int y) => y - 1;
    private static int Down(this int y) => y + 1;
    private static int Left(this int x) => x - 1;
    private static int Right(this int x) => x + 1;
    private const int LowPointRiskOffset = 1;

    private const string HeightMapPath = @"C:\Users\marku\Code\C#\Advent2021\input\day_09.txt";

    public static void ModuleMain()
    {
        Console.WriteLine(File.ReadAllText(HeightMapPath).GenerateHeightMapMatrix().FindLowPoints());
    }

    private static IEnumerable<IEnumerable<int>> GenerateHeightMapMatrix(this string heightMap) =>
        heightMap.Split(Environment.NewLine)
            .Select(line => line.ToCharArray().Select(chr => int.Parse(chr.ToString())));

    private static int FindLowPoints(this IEnumerable<IEnumerable<int>> heightMapList)
    {
        var sumOfLowPoints = 0;
        // Type becomes int[][] here. A matrix of heights.
        var heightMapMatrix = heightMapList.Select(x => x.ToArray()).ToArray();
        // Y is the row we are on.
        var rowLength = heightMapMatrix.Length;
        // X is the column we are on.
        var columnLength = heightMapMatrix[0].Length;
        var areaChecks = new bool[4];

        // set all fields to true.
        for (var i = 0; i < areaChecks.Length; i += 1)
        {
            areaChecks[i] = true;
        }

        for (var row = 0; row < rowLength; row += 1)
        {
            for (var col = 0; col < columnLength; col += 1)
            {
                if (col.Up() >= 0)
                {
                    if (heightMapMatrix[row][col] >= heightMapMatrix[row][col.Up()])
                    {
                        areaChecks[(int) Direction.Up] = false;
                    }
                }

                if (col.Down() < columnLength)
                {
                    if (heightMapMatrix[row][col] >= heightMapMatrix[row][col.Down()])
                    {
                        areaChecks[(int) Direction.Down] = false;
                    }
                }

                if (row.Left() >= 0)
                {
                    if (heightMapMatrix[row][col] >= heightMapMatrix[row.Left()][col])
                    {
                        areaChecks[(int) Direction.Left] = false;
                    }
                }

                if (row.Right() < rowLength)
                {
                    if (heightMapMatrix[row][col] >= heightMapMatrix[row.Right()][col])
                    {
                        areaChecks[(int) Direction.Right] = false;
                    }
                }

                if (areaChecks.All(x => x))
                {
                    sumOfLowPoints += heightMapMatrix[row][col] + LowPointRiskOffset;
                }

                // Reset all the checks to true.
                areaChecks = areaChecks.ResetChecks();
            }
        }

        return sumOfLowPoints;
    }

    private static bool[] ResetChecks(this IEnumerable<bool> checks) => checks.Select(_ => true).ToArray();
}