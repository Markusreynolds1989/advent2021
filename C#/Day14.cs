﻿using System.Text;

namespace Advent2021;

public static class Day14
{
    private const           string FilePath    = @"C:\Users\marku\Code\C#\Advent2021\input\day_14.txt";
    private static readonly Regex  PolymerRule = new(@"(?<rule>\w\w\s)->(?<letter>\s\w)");

    public static void ModuleMain()
    {
        var file = File.ReadAllText(FilePath);
        var lines = file.Split('\n');

        var polymer = lines[0].Trim().ToCharArray().ToList();
        var rules = file.GenerateRules();

        for (var i = 0; i < 10; i += 1)
        {
            polymer.PolymerInsertion(rules);
        }

        var solution = CalculatePolymerSolution(polymer.CountMostCommonElement(), polymer.CountLeastCommonElement());
        Console.WriteLine(solution);
        /* Test */
        // Debug.Assert(solution == TestSolution, $"{solution} != {TestSolution}");
    }

    // This will mutate the state on the polymer list every step.
    private static void PolymerInsertion(this IList<char> polymer, IReadOnlyDictionary<string, char> rules)
    {
        // Need to increment window every time there is an insertion so we don't redo it.
        var count = polymer.Count - 1;
        var window = 0;

        for (var i = 0; i < count; i += 1)
        {
            var key = polymer[i + window].ToString() + polymer[i + 1 + window];
            if (rules.ContainsKey(key))
            {
                window += 1;
                polymer.Insert(i + window, rules[key]);
            }
        }
    }

    private static Dictionary<string, char> GenerateRules(this string input)
    {
        var mapRules = new Dictionary<string, char>();
        var rules = PolymerRule.Matches(input);

        foreach (Match rule in rules)
        {
            mapRules.Add(rule.Groups["rule"].Value.Trim(), rule.Groups["letter"].Value.Trim().ToCharArray()[0]);
        }

        return mapRules;
    }

    private static int CountMostCommonElement(this IEnumerable<char> polymer) =>
        polymer.GroupBy(x => x).Select(n => (n.Key, count: n.Count())).OrderBy(x => x.count).Last().count;

    private static int CountLeastCommonElement(this IEnumerable<char> polymer) =>
        polymer.GroupBy(x => x).Select(n => (n.Key, count: n.Count())).Last().count;

    private static int CalculatePolymerSolution(int mostCommon, int leastCommon) =>
        mostCommon - leastCommon;

    #region TestData

    private const int TestSolution = 1588;

    private const string TestInput = @"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

    #endregion
}