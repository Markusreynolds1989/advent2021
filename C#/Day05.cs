﻿namespace Advent2021;

public static class Day05
{
    private const string Path = @"C:\Users\marku\Code\C#\Advent2021\input\day_05.txt";

    public static void DayMain()
    {
        var inputLines = File.ReadAllLines(Path);

        var rawPoints =
            // Split up the lines by the -> value, this is to separate the x1,y1 and x2, y2 points.
            inputLines
                .Select(x => x.Split("->",
                    StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries))
                .Select(x => x
                    // Split up based on the comma.
                    .Select(y =>
                        y.Split(',', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries))
                    .SelectMany(q => q
                        // Parse all the strings into ints.
                        .Select(int.Parse))
                    .ToList())
                .ToList();

        var lines = GenerateLines(rawPoints).ToList();
        var grid = new Dictionary<(int, int), int>();

        foreach (var line in lines)
        {
            DrawLine(grid, line);
        }

        Console.WriteLine(grid.Count(x => x.Value > 1));
    }

    // Mutate the gird so that we can see which lines overlap which others.
    private static void DrawLine(Dictionary<(int, int), int> grid, Line line)
    {
        if (line.Start.X == line.End.X || line.Start.Y == line.End.Y)
        {
            foreach (var point in line.Points)
            {
                var tryAdd = grid.TryAdd((point.X, point.Y), 1);

                if (tryAdd == false)
                {
                    grid[(point.X, point.Y)] += 1;
                }
            }
        }
    }

    private static IEnumerable<Line> GenerateLines(IReadOnlyList<List<int>> rawPoints)
    {
        var lines = new List<Line>();
        for (var i = 0; i < rawPoints.Count; i += 1)
        {
            var start = new Point(rawPoints[i][0], rawPoints[i][1]);
            var end = new Point(rawPoints[i][2], rawPoints[i][3]);
            lines.Add(new Line(start, end));
        }

        return lines;
    }
}

public struct Point
{
    public int X;
    public int Y;

    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    public override string ToString() =>
        $"({X},{Y})";
}

public struct Line
{
    public readonly Point Start;
    public readonly Point End;
    public readonly List<Point> Points;

    public Line(Point start, Point end)
    {
        Start = start;
        End = end;
        Points = default;
        Points = GeneratePoints();
    }

    // Using the start and end of the line, generate all points that make up the line.
    private List<Point> GeneratePoints()
    {
        var points = new List<Point>();

        // Start = End
        if (Start.X == End.X && Start.Y == End.Y)
        {
            points.Add(new Point(Start.X, Start.Y));
            return points;
        }

        // Start x < end x and start y < end y
        if (Start.X < End.X && Start.Y < End.Y)
        {
            for (var x = Start.X; x <= End.X; x += 1)
            {
                for (var y = Start.Y; y <= End.Y; y += 1)
                {
                    points.Add(new Point(x, y));
                }
            }

            return points;
        }

        // Start x < end x and start y == end y
        if (Start.X < End.X && Start.Y == End.Y)
        {
            for (var x = Start.X; x <= End.X; x += 1)
            {
                points.Add(new Point(x, End.Y));
            }

            return points;
        }

        // start x > end x and start y < end y
        if (Start.X > End.X && Start.Y < End.Y)
        {
            for (var x = Start.X; x >= End.X; x -= 1)
            {
                for (var y = Start.Y; y <= End.Y; y += 1)
                {
                    points.Add(new Point(x, y));
                }
            }

            return points;
        }

        // start x < end x and start y > end y
        if (Start.X < End.X && Start.Y > End.Y)
        {
            for (var x = Start.X; x <= End.X; x += 1)
            {
                for (var y = Start.Y; y >= End.Y; y -= 1)
                {
                    points.Add(new Point(x, y));
                }
            }

            return points;
        }

        // start x = end x and start y < end y
        if (Start.X == End.X && Start.Y < End.Y)
        {
            for (var y = Start.Y; y <= End.Y; y += 1)
            {
                points.Add(new Point(End.X, y));
            }

            return points;
        }

        // start x > end x and start y > end y
        if (Start.X > End.X && Start.Y > End.Y)
        {
            for (var x = Start.X; x >= End.X; x -= 1)
            {
                for (var y = Start.Y; y >= End.Y; y -= 1)
                {
                    points.Add(new Point(x, y));
                }
            }

            return points;
        }

        if (Start.X > End.X && Start.Y == End.Y)
        {
            for (var x = Start.X; x >= End.X; x -= 1)
            {
                points.Add(new Point(x, End.Y));
            }

            return points;
        }

        if (Start.X == End.X && Start.Y > End.Y)
        {
            for (var y = Start.Y; y >= End.Y; y -= 1)
            {
                points.Add(new Point(End.X, y));
                return points;
            }
        }

        return points;
    }

    public override string ToString() =>
        $"(Start: {Start}, End: {End})";
}