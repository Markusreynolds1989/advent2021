mod read_file;
mod day_11;

fn main() {
    println!("{}", day_11::sum_of_flashes());
}
