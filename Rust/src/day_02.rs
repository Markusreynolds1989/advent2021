use crate::read_file::read_file;
use itertools::Itertools;

const INPUT_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2021\input\day_02.txt";

enum Direction {
    Forward,
    Down,
    Up,
    None,
}

pub fn work() -> i32 {
    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;

    let input = read_file(&INPUT_PATH)
        .split('\n')
        .collect::<Vec<&str>>()
        .iter()
        .map(|x| x.split(' ').collect::<Vec<&str>>())
        .collect::<Vec<Vec<&str>>>()
        .iter()
        .map(|x| {
            (
                match_direction(x[0]),
                x[1].trim_end().parse::<i32>().unwrap(),
            )
        })
        .collect::<Vec<(Direction, i32)>>();

    for (dir, number) in input {
        match dir {
            Direction::Forward => {
                horizontal += number;
                depth += aim * number;
            }
            Direction::Up => aim -= number,
            Direction::Down => aim += number,
            _ => (),
        }
    }

    horizontal * depth
}

fn match_direction(input: &str) -> Direction {
    match input.trim_end() {
        "forward" => Direction::Forward,
        "down" => Direction::Down,
        "up" => Direction::Up,
        _ => Direction::None,
    }
}
