use std::fs::File;
use std::io::Read;

pub fn read_file(path: &str) -> String {
    let mut file = File::open(path).unwrap();
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).unwrap();
    // Remove any byte order marks.
    if buffer.contains('\u{feff}') {
        buffer = buffer.replace("\u{feff}", "");
    }
    buffer
}