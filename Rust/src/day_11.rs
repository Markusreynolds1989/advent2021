const OCTOPUS_INIT_LEVELS: &str = r"4525436417
1851242553
5421435521
8431325447
4517438332
3521262111
3331541734
4351836641
2753881442
7717616863";

// Star 1
pub fn sum_of_flashes() -> i32 {
    let mut octopus_levels = convert_octopus_levels_to_matrix(OCTOPUS_INIT_LEVELS);
    sum_of_octopus_flashes(&mut octopus_levels)
}

fn convert_octopus_levels_to_matrix(octopus_levels: &str) -> [[i32; 10]; 10] {
    let mut octopus_matrix: [[i32; 10]; 10] = [[0; 10]; 10];

    let numbers = OCTOPUS_INIT_LEVELS
        .split('\n')
        .map(|x| x.trim())
        .map(|x| x.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();

    for row in 0..numbers.len() {
        for col in 0..numbers[row].len() {
            octopus_matrix[row][col] = numbers[row][col].to_string().parse::<i32>().unwrap();
        }
    }

    octopus_matrix
}

// Brute force check all cells. :(
/*fn pulse_octopuses(octopus_matrix: &mut [[i32; 10]; 10]) -> &mut [[i32; 10]; 10] {
    octopus_matrix
}*/

fn sum_of_octopus_flashes(octopus_matrix: &mut [[i32; 10]; 10]) -> i32 {
    let mut flash_sum = 0;
    // Test 100 times.
    for i in 0..100 {
        for x in 0..10 {
            for y in 0..10 {
                if octopus_matrix[x][y] > 9 {
                    // Flash.
                    // Count flashes.
                    flash_sum += 1;
                    octopus_matrix[x][y] = 0;
                } else {
                    octopus_matrix[x][y] += 1;
                }
            }
        }
    }

    flash_sum
}

/* Helper functions */
fn top_left(x: i32, y: i32) -> (i32, i32) {
    (x - 1, y - 1)
}

fn top(x: i32, y: i32) -> (i32, i32) {
    (x, y - 1)
}

fn top_right(x: i32, y: i32) -> (i32, i32) {
    (x + 1, y - 1)
}

fn left(x: i32, y: i32) -> (i32, i32) {
    (x - 1, y)
}

fn right(x: i32, y: i32) -> (i32, i32) {
    (x + 1, y)
}

fn bottom_left(x: i32, y: i32) -> (i32, i32) {
    (x - 1, y + 1)
}

fn bottom(x: i32, y: i32) -> (i32, i32) {
    (x, y + 1)
}

fn bottom_right(x: i32, y: i32) -> (i32, i32) {
    (x + 1, y + 1)
}

#[cfg(test)]

mod tests {
    use super::*;
    const OCTOPUS_TEST: &str = r"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    #[test]
    fn sum_of_flashes_test() {
        let mut matrix = convert_octopus_levels_to_matrix(OCTOPUS_TEST);
        assert_eq!(sum_of_octopus_flashes(&mut matrix), 1656);
    }
}
