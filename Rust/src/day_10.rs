use crate::read_file::read_file;

const CURVE: usize = 0;
const SQUARE: usize = 1;
const CURLY: usize = 2;
const ARROW: usize = 3;

trait SyntaxScoreSum {
    fn syntax_score_sum(&self) -> i32;
}

trait IncorrectSyntaxCounter {
    fn incorrect_syntax_counter(&self) -> [i32; 4];
}

impl SyntaxScoreSum for [i32; 4] {
    fn syntax_score_sum(&self) -> i32 {
        const CURVE_SCORE: i32 = 3;
        const SQUARE_SCORE: i32 = 57;
        const CURLY_SCORE: i32 = 1197;
        const ARROW_SCORE: i32 = 25137;

        self[CURVE] * CURVE_SCORE
            + self[SQUARE] * SQUARE_SCORE
            + self[CURLY] * CURLY_SCORE
            + self[ARROW] * ARROW_SCORE
    }
}

impl IncorrectSyntaxCounter for Vec<Vec<char>> {
    fn incorrect_syntax_counter(&self) -> [i32; 4] {
        let mut syntax_counter: [i32; 4] = [0, 0, 0, 0];
        let mut syntax_stack: Vec<char> = Vec::new();

        for line in self {
            for syntax in line {
                match syntax {
                    '(' | '[' | '{' | '<' => syntax_stack.push(*syntax),
                    ')' if *syntax_stack.last().unwrap() == '(' => {
                        syntax_stack.pop();
                    }
                    ']' if *syntax_stack.last().unwrap() == '[' => {
                        syntax_stack.pop();
                    }
                    '}' if *syntax_stack.last().unwrap() == '{' => {
                        syntax_stack.pop();
                    }
                    '>' if *syntax_stack.last().unwrap() == '<' => {
                        syntax_stack.pop();
                    }
                    _ => match syntax {
                        ')' => {
                            syntax_counter[CURVE] += 1;
                            break;
                        }
                        ']' => {
                            syntax_counter[SQUARE] += 1;
                            break;
                        }
                        '}' => {
                            syntax_counter[CURLY] += 1;
                            break;
                        }
                        _ => {
                            syntax_counter[ARROW] += 1;
                            break;
                        }
                    },
                }
            }
        }

        syntax_counter
    }
}

// Star 1.
pub fn count_and_sum_syntax_errors() -> i32 {
    const SYNTAX_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2021\input\day_10.txt";
    read_file(SYNTAX_PATH)
        .trim()
        .split('\n')
        .map(|x| x.trim_end())
        .map(|x| x.chars().collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>()
        .incorrect_syntax_counter()
        .syntax_score_sum()
}

#[cfg(test)]
mod tests {
    use crate::day_10::{IncorrectSyntaxCounter, SyntaxScoreSum};
    const _SYNTAX_TEST: &str = r"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

    #[test]
    fn syntax_test() {
        assert_eq!(
            _SYNTAX_TEST
                .trim()
                .split('\n')
                .map(|x| x.chars().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>()
                .incorrect_syntax_counter()
                .syntax_score_sum(),
            26397
        );
    }
}
