use crate::read_file::read_file;

const INPUT_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2021\input\day_03.txt";

pub fn main() -> i32 {
    let input = read_file(INPUT_PATH);
    let input = input
        .split('\n')
        .collect::<Vec<&str>>()
        .iter()
        .map(|x| x.trim_end())
        .map(|x| string_to_bits(x))
        .collect::<Vec<Vec<i32>>>();

    determine_lsr(&input)
}

fn string_to_bits(input: &str) -> Vec<i32> {
    input
        .chars()
        .map(|x| if x == '1' { 1 } else { 0 })
        .collect::<Vec<i32>>()
}

fn bit_adder(input: &Vec<i32>) -> i32 {
    fn bit_adder_loop(input: &Vec<i32>, high_bit: i32, sum: i32, index: i32) -> i32 {
        match index {
            12 => sum,
            _ => match input[index as usize] {
                1 => bit_adder_loop(&input, high_bit / 2, sum + high_bit, index + 1),
                _ => bit_adder_loop(&input, high_bit / 2, sum, index + 1),
            },
        }
    }

    bit_adder_loop(&input, 2048, 0, 0)
}

fn common_bit_calculator(input: &Vec<Vec<i32>>, bit_position: i32, commonality: char) -> i32 {
    let ones: i32 = input.iter().fold(0, |x, y| {
        if y[bit_position as usize] == 1 {
            x + 1
        } else {
            x
        }
    });

    let zeroes: i32 = input.iter().fold(0, |x, y| {
        if y[bit_position as usize] == 0 {
            x + 1
        } else {
            x
        }
    });

    match commonality {
        '<' => {
            if ones >= zeroes {
                1
            } else {
                0
            }
        }
        _ => {
            if ones >= zeroes {
                0
            } else {
                1
            }
        }
    }
}

fn determine_lsr(input: &Vec<Vec<i32>>) -> i32 {
    let mut gsr: Vec<Vec<i32>> = input.clone();
    let mut csr: Vec<Vec<i32>> = input.clone();

    for index in 0..12 {
        if !(gsr.len() < (2 as usize)) {
            gsr = gsr
                .iter()
                .filter(|x| x[index as usize] == common_bit_calculator(&gsr, index, '>'))
                .map(|x| x.to_owned())
                .collect::<Vec<Vec<i32>>>();
        }
    }

    for index in 0..12 {
        if !(csr.len() < (2 as usize)) {
            csr = csr
                .iter()
                .filter(|x| x[index as usize] == common_bit_calculator(&csr, index, '<'))
                .map(|x| x.to_owned())
                .collect::<Vec<Vec<i32>>>();
        }
    }

    bit_adder(&gsr[0]) * bit_adder(&csr[0])
}
