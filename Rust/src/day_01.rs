use itertools::Itertools;
use std::fs::File;
use std::io::Read;

const INPUT_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2021\input\day_01.txt";

fn read_file() -> String {
    let mut file = File::open(INPUT_PATH).unwrap();
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).unwrap();
    buffer
}

fn work(input: &str) -> i32 {
    let lines: Vec<&str> = input.split('\n').collect();
    let mut current: i32 = lines[0].trim_end().parse::<i32>().unwrap();
    let mut result = 0;

    for x in 1..lines.len() {
        // Parse the current line.
        let line = lines[x].trim_end().parse::<i32>().unwrap();

        if current < line {
            result += 1;
            current = line
        } else if current > line {
            current = line
        }
    }

    result
}

fn work_sliding_functional(input: &str) -> i32 {
    input
        // Split up the string by lines.
        .split('\n')
        // Trim up the lines as they have a blank space in them.
        .map(|x| x.trim_end())
        // Parse all the lines. Dangerous but we know the input is i32.
        .map(|x| x.parse::<i32>().unwrap())
        // Use tuple_windows from the itertools library to keep it functional.
        .tuple_windows::<(i32, i32, i32)>()
        // Map out the results of summing up the tuples.
        .map(|x| x.0 + x.1 + x.2)
        // Tuple up the results again for comparing first and second.
        .tuple_windows::<(i32, i32)>()
        .fold(0, |mut x: i32, y: (i32, i32)| {
            if y.0 < y.1 {
                x += 1
            }
            x
        })
}

fn work_sliding_window(input: &str) -> i32 {
    let mut result = 0;
    let mut next = 0;

    // Map up the lines so we trim them at the same time we split them up.
    let lines: Vec<i32> = input
        .split('\n')
        .map(|x| x.trim_end())
        .map(|x| x.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();

    let mut current = lines[0] + lines[1] + lines[2];

    // Only go up to length - 2 as we compare every 3 items.
    for x in 1..lines.len() - 2 {
        next = lines[x] + lines[x + 1] + lines[x + 2];

        if current < next {
            result += 1;
            current = next;
        } else if current > next {
            current = next;
        }
    }

    result
}

pub fn count_increase() -> i32 {
    work_sliding_functional(&read_file())
}
