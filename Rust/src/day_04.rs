use std::collections::HashMap;
use crate::read_file::read_file;

const BOARDS_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2021\input\day_04.txt";

// Star 1
pub fn sum_of_first_winning_board() -> i32 {
    0
}

// Star 2
pub fn sum_of_last_winning_board() -> i32 {
    0
}

fn call_number(number: i32, boards: &Vec<HashMap<i32, bool>>){

}

fn sum_unmarked_numbers(board: &HashMap<i32,bool>) -> i32{
    let mut sum = 0;
    for (number, called) in board {
        if *called {
            sum += *number
        }
    }

    sum
}

#[cfg(test)]
mod tests {
    use crate::day_04::{sum_of_first_winning_board, BOARDS_PATH};

    const TEST_BOARDS: &str =
    r"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";

    #[test]
    fn test_answer(){
        assert_eq!(sum_of_first_winning_board(), 4512);
    }

    #[test]
    fn real_answer() {
        assert_eq!(sum_of_first_winning_board(), 39902);
    }
}