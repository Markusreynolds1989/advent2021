use crate::read_file::read_file;
use std::fs::{read, File};
use std::io::Write;
use std::os::windows::fs::FileExt;
use std::str::FromStr;

const INPUT_PATH: &str = r"C:\Users\marku\Code\Rust\advent_2021\input\day_06.txt";
// const INPUT_TEST: [i32; 5] = [3, 4, 3, 1, 2];

pub fn main() -> i32 {
    let input = read_file(INPUT_PATH);

    let mut fish = input
        .trim_end()
        .split(',')
        .map(|x| x.trim())
        .map(|x| u8::from_str(x).unwrap())
        .collect::<Vec<u8>>();

    let mut counter = fish.len();
    let mut cursor_one: usize = 8;
    let mut cursor_two: usize = 8;
    let mut add_counter: usize = 0;

    let mut ocean =
        File::create(r"C:\Users\marku\Code\Rust\advent_2021\target\debug\ocean.txt").unwrap();

    for x in 0..counter {
        ocean.write(&fish);
    }

    let write_out = reader(&ocean, cursor_one, &mut counter, &mut add_counter);
    writer(&mut ocean, cursor_two, write_out);

    let mut test = read(r"C:\Users\marku\Code\Rust\advent_2021\target\debug\ocean.txt").unwrap();
    println!("{:?}", test);

    counter as i32
}

fn reader(input: &File, cursor: usize, counter: &mut usize, add_counter: &mut usize) -> u8 {
    let mut buffer: Vec<u8> = Vec::new();
    input.seek_read(&mut *buffer, cursor as u64);

    if buffer[0] == 0 {
        *add_counter += 1;
        *counter += 1;
    }

    buffer[cursor] - 1
}

fn writer(input: &mut File, cursor: usize, to_write: u8) {
    input.seek_write(&[to_write], cursor as u64);
}

/* Day 01
    let mut fish = input
        .trim_end()
        .split(',')
        .map(|x| x.trim())
        .map(|x| x.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();

    let mut counter = fish.len();

    for x in 1..81 {
        recalculate_fish(&mut fish, &mut counter);
    println!("Day: {}, Count: {}", x, counter);
    }

    counter as i32
}

fn recalculate_fish(fish: &mut Vec<i32>, counter: &mut usize) {
    for x in 0..*counter {
        if fish[x] == 0 {
            fish.push(8);
            *counter += 1;
        }

        fish[x] -= 1;

        if fish[x] == -1 {
            fish[x] = 6;
        }
    }
}*/
