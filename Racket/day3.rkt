#lang racket

(require rackunit threading)

(define data-path "..\\Data\\day_03.txt")
(define test-data (list "00100" "11110" "10110" "10111" "10101" "01111" "00111" "11100" "10000" "11001" "00010" "01010"))

(define (char->bit character)
  (match character
    [#\0 0]
    [#\1 1]))

(define (string-list->bits input)
  (~>> input
       ; Convert each string into a list.
       (map (λ (item) (string->list item)))
       ; Convert each list of chars into a list of bits.
       (map (λ (list) (map (λ (item) (char->bit item)) list)))))

;;; Take the bits and split them up by column instead of row.
(define (col-bit-list bits index)
  (map (λ (bit-list) (append (list-ref bit-list index))) bits))

(define (col-bits bits)
  (map (λ (index) (col-bit-list bits index)) (stream->list (in-range (length (first bits))))))

(define (common-bit bit-list cardinality)
  (match cardinality
    ['>
     (if (>= (length (filter (λ (bit) (= bit 1)) bit-list))
             (length (filter (λ (bit) (= bit 0)) bit-list)))
         1
         0)]
    ['<
     (if (>= (length (filter (λ (bit) (= bit 1)) bit-list))
             (length (filter (λ (bit) (= bit 0)) bit-list)))
         0
         1)]))

(define (generate-ε bit-lists)
  (map (λ (bit-list) (append (common-bit bit-list '>))) bit-lists))

(define (generate-ɣ bit-lists)
  (map (λ (bit-list) (append (common-bit bit-list '<))) bit-lists))

(define (generate-range-list start end step)
  (define (loop start end)
    (if (< start end)
        (append
         (list (step start))
         (loop (* start 2) end)) '()))
  (cons start (loop start end)))

(define (bit-list->number bit-list max-bit)
  (foldl (λ (bit bit-num result) (if (= 1 bit) (+ result bit-num) result))
         0
         bit-list
         (reverse (generate-range-list 1 max-bit (λ (x) (* x 2))))))

; Star One.
(define (parse-bits data max)
  (define bit-lists
    (~> data
        string-list->bits
        col-bits))

  (* (~> bit-lists generate-ε (bit-list->number max))
     (~> bit-lists generate-ɣ (bit-list->number max))))

(define (bit-adder bit-list)
  (define (bit-adder-loop bit-list high-bit sum index)
    (match index
      [12 sum]
      [_
       (match (list-ref bit-list index)
         [1 (bit-adder-loop bit-list (/ high-bit 2) (+ sum high-bit) (+ 1 index))]
         [_ (bit-adder-loop bit-list (/ high-bit 2) sum (+ 1 index))])]))
  (bit-adder-loop bit-list 2048 0 0))

; Get the ogr here, the ogr is the most common bit and then the byte that is the most common, if it is a 1 or it to the bits. If it is a zero, or it to the bits.
(define (generate-ogr bits-list [index 0])
  (printf "index ~a\n" index)
  (printf "size: ~a\n" (length bits-list))
  (printf "inner-size: ~a\n" (length bits-list))
  (printf "first: ~a\n" (first bits-list))
  (if (or (< (length bits-list) 2)
          (= index 12))
      '()
      (append
       (generate-ogr
        (filter (lambda (value) (= (list-ref value index) (common-bit (first bits-list) '>))) bits-list)
        (+ 1 index)))))

; csr, same as above but for lower numbers.
(define (generate-csr bits-list [index 0])
  (printf "index: ~a\n" index)
  (printf "size: ~a\n" (length bits-list))
  (printf "inner-size: ~a\n" (length (first bits-list)))
  (if (or (< (length bits-list) 2)
          (= index 12))
      '()
      (append
       (generate-csr
        (filter (lambda (value) (= (list-ref value index) (common-bit (first bits-list) '<))) bits-list)
        (+ 1 index)))))

; Star Two.
(define (parse-bits-complex data max)
  (define bit-lists
    (~> data
        string-list->bits
        col-bits))
  (* (~> bit-lists generate-ogr bit-adder first)
     (~> bit-lists generate-csr bit-adder first)))

(check-equal? (parse-bits test-data 16) 198)
(check-equal? (parse-bits (file->lines data-path) 2048) 3912944)

(check-equal? (bit-adder (list 0 0 0 0 1 1 1 1 1 1 1 1)) 255)
(check-equal? (bit-adder (list 0 0 0 0 0 0 0 0 0 0 0 1)) 1)

(check-equal? (parse-bits-complex test-data 16) 230)
;(check-equal? (parse-bits-complex (file->lines data-path) 2048) 4996233)