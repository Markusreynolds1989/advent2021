module day_08;

import std.algorithm : map, splitter, sort, all, fold, sum;
import std.algorithm.iteration;
import std.array : array;
import std.conv : to;
import std.range : chain, only, take, drop;
import std.stdio : File, writeln;
import std.string : split, stripLeft;

static enum string puzzleSignalPatternsPath = `C:\Users\marku\Code\D\advent_2021\source\input\day_08.txt`;

// Star 2, basically create a cypher.

// Instructions Star 1
// How many times do the digits 1 4 7 or 8 appear?
/+ 
    2 letter combo                1
    3 letter combo                7
    4 letter combo                4
    7 letter combo                8
    +/

public static void moduleMain()
{
    // Star 1.
    auto puzzleSignalPatterns =
        File(puzzleSignalPatternsPath, "r")
        .byLine()
        .map!(x => 
            x
            .split('|')[1]
            .stripLeft
            .to!string
            .split(' '))
        .array();

    auto signalOccurances = 0;

    foreach (signal; puzzleSignalPatterns)
    {
        signalOccurances += signal.countNumberOccurances();
    }

    writeln(signalOccurances);
}

// Star 1.
private static int countNumberOccurances(const ref string[] digits) pure
{
    int counter = 0;

    foreach (ref digit; digits)
    {
        if (digit.length == 2 || digit.length == 3 || digit.length == 4 || digit.length == 7)
        {
            counter += 1;
        }
    }

    return counter;
}
