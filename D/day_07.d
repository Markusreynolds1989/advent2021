module day_07;
import std.stdio, std.algorithm.iteration;
import std.range : chain, only;
import std.array : array;
import std.conv : to;
import std.algorithm : map, splitter, sort;

void main(string[] args)
{
    /// Star One:

    // int[] testCrabs = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14];

    // Brute force algorithm:
    // Sort the array, then take max number and subtract it from each element.
    // Take the difference and sum that up.
    // Sort the results and take the first item from the list as the minimum.

    enum string inputPath = `C:\Users\marku\Code\D\advent_2021\src\input\day_07.input`;

    // writeln(testCrabs);
    auto file = File(inputPath, "r");

    string input;

    file.readf("%s", input);

    // List that will hold all the numbers we will need to sort.
    auto numberSplit = array(splitter(input, ',').map!(to!int)).sort!"b > a";

    int length = numberSplit.length;
    int high = numberSplit[length - 1];

    // List we will need the min from, push all the sums here.
    int[] results = new int[0];

    for (int i = high; i >= 0; i -= 1)
    {
        auto sum = 0;
        auto sum cost = 1;

        for (int y = length - 1; y >= 0; y -= 1)
        {
            auto temp = i - numberSplit[y];
            if (temp < 0)
            {
                temp *= -1;
            }

            sum += temp;
        }

        if (sum != 0)
        {
            results ~= sum;
        }

        sum = 0;
    }

    results.sort!"b > a";

    writeln(results[0]);
}
