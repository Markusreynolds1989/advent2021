module Advent2021.day03
open System
open System.IO

let path =
    "C:\Users\marku\Code\F#\Advent2021\day_03.txt"

let stringToBits (input: string) : array<int> =
    input
    |> Seq.map (fun x -> if x = '1' then 1 else 0)
    |> Seq.toArray

let bitAdder (input: array<int>) : int =
    let rec bitAdderLoop (input: seq<int>) (highBit: int) (sum: int) (index: int) =
        match index with
        | 12 -> sum
        | _ ->
            match Seq.item index input with
            | 1 -> bitAdderLoop input (highBit / 2) (sum + highBit) (index + 1)
            | _ -> bitAdderLoop input (highBit / 2) sum (index + 1)

    // 2048 is the highest bit we need (the 12th bit).
    bitAdderLoop input 2048 0 0

let commonBitCalculator (input: array<array<int>>) (bitPosition: int) (commonality: char) : int =
    let ones: int =
        input
        |> Array.fold (fun x y -> if y.[bitPosition] = 1 then x + 1 else x) 0

    let zeroes: int =
        input
        |> Array.fold (fun x y ->
            if y.[bitPosition] = 0 then x + 1 else x) 0

    match commonality with
    // Most common bit.
    | '<' -> if ones >= zeroes then 1 else 0
    // Least common bit.
    | _ -> if ones >= zeroes then 0 else 1

let determineLsr (input: array<array<int>>) : int =
    let rec determineLsrLoop (input: array<array<int>>) (index: int) (commonality: char) : array<array<int>> =
        match index with
        | 11 -> input
        | x when Array.length input < 2 -> input
        | _ ->
            determineLsrLoop
                (Array.filter (fun x -> x.[index] = (commonBitCalculator input index commonality)) input)
                (index + 1)
                commonality

    let gsr = determineLsrLoop input 0 '>'

    let csr = determineLsrLoop input 0 '<'

    (bitAdder (Seq.item 0 gsr))
    * (bitAdder (Seq.item 0 csr))

let file =
    File.ReadAllLines(path)
    |> Array.map (fun x -> x.TrimEnd())
    |> Array.map stringToBits